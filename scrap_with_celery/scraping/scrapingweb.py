"""
    Run me: celery -A scrapingweb worker --autoscale=5,2
"""
import requests
from bs4 import BeautifulSoup
from celery import Celery
from pymongo import MongoClient
from celery.signals import celeryd_init

app = Celery('tasks', broker='redis://localhost:6379/0')


@celeryd_init.connect()
def init(**kwargs):
    print('Init...')
    get_next_pages.delay()


@app.task
def get_next_pages():
    page_link = 'https://vnexpress.net/tin-tuc/giao-duc/page/1.html'
    nx_pages = '1'

    while nx_pages:
        r = requests.get(page_link)
        soup = BeautifulSoup(r.content, 'lxml')

        # Tim tung bai bao
        heading_news = soup.find_all('article', class_='list_news')

        for i in heading_news:
            # Lay link cua tung bai bao
            new_link = i.a.get('href')
            scrap_vn_express.delay(new_link)

        # Tim cac tag next de chuyen trang
        nx_pages = soup.find_all('a', class_='next')

        for j in nx_pages:
            # Lay link cua cac tag next
            nx = j.get('href')
            page_link = 'https://vnexpress.net' + nx


@app.task
def scrap_vn_express(new_link):

    client = MongoClient('localhost', 27017)
    db = client.vnexpress
    collection = db.vnexpress_collection

    print('scrap url {}'.format(new_link))

    # Kiem tra link cac bai bao bi trung lap trong DB
    if db.vnexpress_collection.find_one(
            {'link': new_link}) is None:
        links = requests.get(new_link)
        soup1 = BeautifulSoup(links.content, 'lxml')

        # Neu tim thay 'section' va class 'sidebar_1'
        if soup1.find('section', class_='sidebar_1'):
            title = soup1.find(class_='title_news_detail mb10').text.replace('\n', '')
            des = soup1.find(class_='description').text.replace('\n', '')
            # Neu tim thay 'article'
            if soup1.article:
                contents = soup1.article.text.replace('\n', '')
            # Neu khong tim thay 'article'
            else:
                get_all_text = soup1.find_all('p', class_='Normal')
                contents = ''
                for tx in get_all_text:
                    contents += ''.join(tx.text.replace('\n', ''))
            # Luu vao DB
            db.vnexpress_collection.insert(
                {'title': title, 'des': des, 'contents': contents, 'link': new_link})

        # Neu tim thay 'section' va class 'top_detail clearfix'
        elif soup1.find('section', class_='top_detail clearfix'):
            title = soup1.find(class_='title').text.replace('\n', '')
            des = soup1.find(class_='lead_detail').text.replace('\n', '')
            contents = soup1.find('video').get('src')
            # Luu vao DB
            db.vnexpress_collection.insert(
                {'title': title, 'des': des, 'contents': contents, 'link': new_link})

        # Neu tim thay 'section' va class 'container infographics'
        elif soup1.find('section', class_='container infographics'):
            title = soup1.find('title_news_detail mb10').text.replace('\n', '')
            des = soup1.find('description').text.replace('\n', '')
            contents = soup1.find('img').get('src')
            # Luu vao DB
            db.vnexpress_collection.insert(
                {'title': title, 'des': des, 'contents': contents, 'link': new_link})
